/*
 * Copyright 2018 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.datenwort.pocoFx.behaviours;

import javafx.geometry.Orientation;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This behaviour can be applied on a created list cell and will setup the drag and drop processing to reorder the items. <br />
 *
 * @param <T> type corresponding to the list view type
 */
public class DragAndDropReorder<T>
{
    private final String dragDropPrefix = java.util.UUID.randomUUID().toString() + ":";

    /**
     * Call with your cell to setup the drag and drop behaviour to reorder the items.
     *
     * @param elements
     * @param delegateCell
     * @param afterReorderAction
     */
    public void setupDragDropOnListCell(
        ListView<T> elements,
        ListCell<T> delegateCell,
        Runnable afterReorderAction)
    {
        delegateCell.setOnDragDetected(event -> handleOnDragDetected(elements, delegateCell, event));

        delegateCell.setOnDragOver(event -> handleOnDragOver(elements, delegateCell, event));

        delegateCell.setOnDragEntered(event -> handleOnDragEntered(elements, delegateCell, event));

        delegateCell.setOnDragExited(event -> handleOnDragExited(elements, delegateCell, event));

        delegateCell.setOnDragDropped(event -> handleOnDragDropped(elements, delegateCell, afterReorderAction, event));

        delegateCell.setOnDragDone(event -> handleOnDragDone(event));
    }

    protected void handleOnDragDropped(ListView<T> elements, ListCell<T> delegateCell, Runnable afterReorderAction, DragEvent event)
    {
        Dragboard db = event.getDragboard();
        boolean success = false;

        if (db.hasString()
            && event.getDragboard().getString().startsWith(dragDropPrefix))
        {
            // collect all the items selected for drag and drop
            List<T> selectedItems = Stream.of(db.getString().substring(dragDropPrefix.length()).split(","))
                .map(indexStr -> Integer.parseInt(indexStr, 10))
                .map(index -> elements.getItems().get(index))
                .collect(Collectors.toList());

            // search the first item equals or after this cell which is not selected.
            // This will be the cell we have to reinsert the data later.
            T insertBeforeElement = delegateCell.getItem();
            int thisIdx = elements.getItems().indexOf(insertBeforeElement);
            while (selectedItems.contains(insertBeforeElement))
            {
                thisIdx++;
                if (thisIdx >= elements.getItems().size())
                {
                    // no element found. will add the dragged items at the end of the list
                    insertBeforeElement = null;
                    break;
                }

                insertBeforeElement = elements.getItems().get(thisIdx);
            }

            // remove all items from the list ...
            elements.getItems().removeAll(selectedItems);
            // and find the index of the element we have to insert before.
            thisIdx = insertBeforeElement == null ? -1 : elements.getItems().indexOf(insertBeforeElement);
            if (thisIdx >= 0)
            {
                elements.getItems().addAll(thisIdx, selectedItems);
            }
            else
            {
                elements.getItems().addAll(selectedItems);
            }

            success = true;

            // JavaFX will keep the selected indexes, but we will keept the initial items selected.
            elements.getSelectionModel().clearSelection();
            selectedItems.forEach(selectedItem -> elements.getSelectionModel().select(selectedItem));

            if (afterReorderAction != null)
            {
                afterReorderAction.run();
            }
        }

        event.setDropCompleted(success);
        event.consume();
    }

    protected void handleOnDragExited(ListView<T> elements, ListCell<T> delegateCell, DragEvent event)
    {
        if (event.getDragboard().hasString()
            && event.getDragboard().getString().startsWith(dragDropPrefix))
        {
            while (delegateCell.getStyleClass().remove("multiListEdit_insertAbove_horizontal"))
            {
                ;
            }
            while (delegateCell.getStyleClass().remove("multiListEdit_insertAbove_vertical"))
            {
                ;
            }
        }
    }

    protected void handleOnDragEntered(ListView<T> elements, ListCell<T> delegateCell, DragEvent event)
    {
        if (event.getDragboard().hasString()
            && event.getDragboard().getString().startsWith(dragDropPrefix))
        {
            delegateCell.getStyleClass().add("multiListEdit_insertAbove_" + (elements.getOrientation() == Orientation.VERTICAL ? "vertical" : "horizontal"));
        }
    }

    protected void handleOnDragOver(ListView<T> elements, ListCell<T> delegateCell, DragEvent event)
    {
        if (event.getDragboard().hasString()
            && event.getDragboard().getString().startsWith(dragDropPrefix))
        {
            event.acceptTransferModes(TransferMode.MOVE);
        }

        event.consume();
    }

    protected void handleOnDragDetected(ListView<T> elements, ListCell<T> delegateCell, MouseEvent event)
    {
        // create a string containing all indexes of the selected items ...
        String dragString = elements.getSelectionModel().getSelectedIndices().stream()
            .map(Object::toString)
            .collect(Collectors.joining(","));

        if (dragString == null || dragString.isEmpty())
        {
            return;
        }

        Dragboard dragboard = elements.startDragAndDrop(TransferMode.MOVE);
        ClipboardContent content = new ClipboardContent();
        // ... and prefix with the dragDropPrefix used to uniquely identify the drag request.
        content.putString(dragDropPrefix + dragString);
        try
        {
            delegateCell.setOpacity(0.1);
            dragboard.setDragView(delegateCell.snapshot(null, null));
        }
        finally
        {
            delegateCell.setOpacity(1);
        }
        dragboard.setContent(content);

        event.consume();
    }

    protected void handleOnDragDone(DragEvent event)
    {
        event.consume();
    }
}
