/*
 * Copyright 2018 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.datenwort.pocoFx.multiValueEdit;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

public class MultiValueEditEvent extends Event
{
    public static final EventType<MultiValueEditEvent> ADD_ACTION = new EventType<MultiValueEditEvent>(Event.ANY, "ADD_ACTION");
    public static final EventType<MultiValueEditEvent> REMOVE_ACTION = new EventType<MultiValueEditEvent>(Event.ANY, "REMOVE_ACTION");
    public static final EventType<MultiValueEditEvent> REORDERED_ACTION = new EventType<MultiValueEditEvent>(Event.ANY, "REORDERED_ACTION");

    public MultiValueEditEvent(Object source, EventTarget target, EventType<MultiValueEditEvent> eventType)
    {
        super(source, target, eventType);
    }

    @Override
    public MultiValueEditEvent copyFor(Object newSource, EventTarget newTarget)
    {
        return (MultiValueEditEvent) super.copyFor(newSource, newTarget);
    }

    @SuppressWarnings("unchecked")
    @Override
    public EventType<? extends MultiValueEditEvent> getEventType()
    {
        return (EventType<? extends MultiValueEditEvent>) super.getEventType();
    }
}
