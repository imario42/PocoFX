/*
 * Copyright 2018 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.datenwort.pocoFx.multiValueEdit;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.util.Callback;

/**
 * This component allows to add or remove elements from a list and reorder them using drag and drop.
 */
public class MultiValueEdit<T> extends Control
{
    private ObjectProperty<ObservableList<T>> items;
    private final ObservableList<T> selectedItems = FXCollections.observableArrayList();

    public MultiValueEdit()
    {
        this(FXCollections.observableArrayList());
    }

    public MultiValueEdit(ObservableList<T> items)
    {
        setItems(items);
    }

    /**
     * Custom cell factory used by the list view.
     * If no cell factory has been set, a simple default cell view will be used.
     */
    private final ObjectProperty<Callback<ListView<T>, ListCell<T>>> elementCellFactory = new SimpleObjectProperty<>();

    /**
     * @see #elementCellFactory
     * @return the cell factory
     */
    public Callback<ListView<T>, ListCell<T>> getElementCellFactory()
    {
        return elementCellFactory.get();
    }

    /**
     * @see #elementCellFactory
     * @return the property holding the cell factory
     */
    public ObjectProperty<Callback<ListView<T>, ListCell<T>>> elementCellFactoryProperty()
    {
        return elementCellFactory;
    }

    /**
     * @see #elementCellFactory
     * @param elementCellFactory the custom cell factory
     */
    public void setElementCellFactory(Callback<ListView<T>, ListCell<T>> elementCellFactory)
    {
        this.elementCellFactory.set(elementCellFactory);
    }


    /**
     * The action executed when pressing the "add" button.
     */
    private ObjectProperty<EventHandler<MultiValueEditEvent>> onAddElementAction = new ObjectPropertyBase<>()
    {
        @Override
        protected void invalidated()
        {
            setEventHandler(MultiValueEditEvent.ADD_ACTION, get());
        }

        @Override
        public Object getBean()
        {
            return MultiValueEdit.this;
        }

        @Override
        public String getName()
        {
            return "onAddElementAction";
        }
    };

    /**
     * @see #onAddElementAction
     * @return the action
     */
    public final ObjectProperty<EventHandler<MultiValueEditEvent>> onAddElementActionProperty()
    {
        return onAddElementAction;
    }

    /**
     * @see #onAddElementAction
     * @param onAddElementAction the action
     */
    public final void setOnAddElementAction(EventHandler<MultiValueEditEvent> onAddElementAction)
    {
        onAddElementActionProperty().set(onAddElementAction);
    }

    /**
     * @see #onAddElementAction
     * @return the action
     */
    public final EventHandler<MultiValueEditEvent> getOnAddElementAction()
    {
        return onAddElementActionProperty().get();
    }



    /**
     * The action executed when pressing the "remove" button.
     */
    private ObjectProperty<EventHandler<MultiValueEditEvent>> onRemoveElementAction = new ObjectPropertyBase<>()
    {
        @Override
        protected void invalidated()
        {
            setEventHandler(MultiValueEditEvent.REMOVE_ACTION, get());
        }

        @Override
        public Object getBean()
        {
            return MultiValueEdit.this;
        }

        @Override
        public String getName()
        {
            return "onRemoveElementAction";
        }
    };

    /**
     * @see #onRemoveElementAction
     * @return the action
     */
    public final ObjectProperty<EventHandler<MultiValueEditEvent>> onRemoveElementActionProperty()
    {
        return onRemoveElementAction;
    }

    /**
     * @see #onRemoveElementAction
     * @param onRemoveElementAction the action
     */
    public final void setOnRemoveElementAction(EventHandler<MultiValueEditEvent> onRemoveElementAction)
    {
        onRemoveElementActionProperty().set(onRemoveElementAction);
    }

    /**
     * @see #onRemoveElementAction
     * @return the action
     */
    public final EventHandler<MultiValueEditEvent> getOnRemoveElementAction()
    {
        return onRemoveElementActionProperty().get();
    }


    /**
     * The action executed after reordering completed.
     */
    private ObjectProperty<EventHandler<MultiValueEditEvent>> onReorderAction = new ObjectPropertyBase<>()
    {
        @Override
        protected void invalidated()
        {
            setEventHandler(MultiValueEditEvent.REORDERED_ACTION, get());
        }

        @Override
        public Object getBean()
        {
            return MultiValueEdit.this;
        }

        @Override
        public String getName()
        {
            return "onReorderAction";
        }
    };

    /**
     * @see #onReorderAction
     * @return the action
     */
    public final ObjectProperty<EventHandler<MultiValueEditEvent>> onReorderActionProperty()
    {
        return onReorderAction;
    }

    /**
     * @see #onReorderAction
     * @param onReorderAction the action
     */
    public final void setOnReorderAction(EventHandler<MultiValueEditEvent> onReorderAction)
    {
        onReorderActionProperty().set(onReorderAction);
    }

    /**
     * @see #onReorderAction
     * @return the action
     */
    public final EventHandler<MultiValueEditEvent> getonReorderAction()
    {
        return onReorderActionProperty().get();
    }



    @Override
    protected Skin<?> createDefaultSkin()
    {
        return new MultiValueEditSkin<>(this);
    }

    /**
     * The items to show in the list view.
     *
     * @return the items
     */
    public ObservableList<T> getItems()
    {
        return items != null ? items.get() : null;
    }

    /**
     * The property holding the items list.
     * @return the property
     */
    public ObjectProperty<ObservableList<T>> itemsProperty()
    {
        if (items == null)
        {
            items = new SimpleObjectProperty<>(this, "items");
        }
        return items;
    }

    /**
     * Items the component should show.
     * @param items the items
     */
    public void setItems(ObservableList<T> items)
    {
        itemsProperty().set(items);
    }

    /**
     * The selected items.
     * @return selected items
     */
    public ObservableList<T> getSeletedItems()
    {
        return selectedItems;
    }
}
