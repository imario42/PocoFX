/*
 * Copyright 2018 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.datenwort.pocoFx.multiValueEdit;

import at.datenwort.pocoFx.behaviours.DragAndDropReorder;
import javafx.beans.InvalidationListener;
import javafx.beans.WeakInvalidationListener;
import javafx.collections.ListChangeListener;
import javafx.collections.WeakListChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.net.URL;

/**
 * Skin to view a list view and two buttons to add and remove elements.
 *
 * @param <T> the item type of the list view
 */
public class MultiValueEditSkin<T> extends SkinBase<MultiValueEdit<T>>
{
    private VBox pane;
    private ListView<T> elements;
    private HBox buttonBar;
    private Button addElementButton;
    private Button removeElementButton;

    private final InvalidationListener itemsChangeListener = observable -> updateListViewItems();

    private final ListChangeListener<T> listViewItemsListener = c -> updateListViewItems();
    private final WeakListChangeListener<T> weakListViewItemsListener = new WeakListChangeListener<T>(listViewItemsListener);

    private final DragAndDropReorder<T> dragAndDropReorder = new DragAndDropReorder<T>();

    public MultiValueEditSkin(MultiValueEdit<T> control)
    {
        super(control);

        elements = new ListView<>();
        VBox.setVgrow(elements, Priority.ALWAYS);
        elements.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        elements.getStyleClass().add("elements");
        elements.setCellFactory(dndCellFactoryWrapper((listView) -> createDefaultCellImpl()));

        addElementButton = new Button("+")
        {
            @Override
            public void fire()
            {
                if (!isDisabled())
                {
                    fireEvent(new MultiValueEditEvent(addElementButton, getSkinnable(), MultiValueEditEvent.ADD_ACTION));
                }
            }
        };
        addElementButton.getStyleClass().add("leftButton");
        addElementButton.setMinSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        addElementButton.setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);

        removeElementButton = new Button("-")
        {
            @Override
            public void fire()
            {
                if (!isDisabled())
                {
                    fireEvent(new MultiValueEditEvent(addElementButton, getSkinnable(), MultiValueEditEvent.REMOVE_ACTION));
                }
            }
        };

        removeElementButton.getStyleClass().add("rightButton");
        removeElementButton.setMinSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);
        removeElementButton.setMaxSize(Region.USE_PREF_SIZE, Region.USE_PREF_SIZE);

        buttonBar = new HBox(addElementButton, removeElementButton);
        VBox.setVgrow(buttonBar, Priority.NEVER);
        buttonBar.getStyleClass().add("buttonBar");

        pane = new VBox(elements, buttonBar);

        URL stylesheet = MultiValueEdit.class.getResource("MultiValueEdit.css");

        pane.getStylesheets().add(stylesheet.toExternalForm());
        pane.getStyleClass().add("multiListEdit");

        getChildren().add(pane);

        elements.itemsProperty().addListener(new WeakInvalidationListener(itemsChangeListener));
        updateListViewItems();

        elements.getSelectionModel().getSelectedItems().addListener((InvalidationListener) observable -> getSkinnable().getSeletedItems().setAll(elements.getSelectionModel().getSelectedItems()));

        getSkinnable().elementCellFactoryProperty().addListener(((observable, oldValue, newValue) -> elements.setCellFactory(dndCellFactoryWrapper(newValue))));

    }

    private Callback<ListView<T>, ListCell<T>> dndCellFactoryWrapper(final Callback<ListView<T>, ListCell<T>> delegate)
    {
        return (listView) -> {
            ListCell<T> delegateCell = delegate.call(listView);

            dragAndDropReorder.setupDragDropOnListCell(elements, delegateCell, () -> {
                getSkinnable().fireEvent(new MultiValueEditEvent(getSkinnable(), getSkinnable(), MultiValueEditEvent.REORDERED_ACTION));
            });
            return delegateCell;
        };
    }

    protected <T> ListCell<T> createDefaultCellImpl()
    {
        return new ListCell<T>()
        {
            @Override
            public void updateItem(T item, boolean empty)
            {
                super.updateItem(item, empty);

                if (empty)
                {
                    setText(null);
                    setGraphic(null);
                }
                else if (item instanceof Node)
                {
                    setText(null);
                    Node currentNode = getGraphic();
                    Node newNode = (Node) item;
                    if (currentNode == null || !currentNode.equals(newNode))
                    {
                        setGraphic(newNode);
                    }
                }
                else
                {
                    setText(item == null ? "null" : item.toString());
                    setGraphic(null);
                }
            }
        };
    }



    private void updateListViewItems()
    {
        elements.getItems().removeListener(weakListViewItemsListener);

        this.elements.setItems(getSkinnable().getItems());

        elements.getItems().addListener(weakListViewItemsListener);

        getSkinnable().requestLayout();
    }
}
