module at.datenwort.pocoFx
{
    requires javafx.base;
    requires javafx.controls;
    requires static controlsfx;

    exports at.datenwort.pocoFx.multiValueEdit;
}