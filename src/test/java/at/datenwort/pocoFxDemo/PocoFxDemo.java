/*
 * Copyright 2018 Mario Ivankovits
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package at.datenwort.pocoFxDemo;

import at.datenwort.pocoFx.multiValueEdit.MultiValueEdit;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.controlsfx.control.PropertySheet;
import org.controlsfx.property.BeanPropertyUtils;
import org.controlsfx.property.editor.DefaultPropertyEditorFactory;
import org.controlsfx.property.editor.PropertyEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PocoFxDemo extends Application
{
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        PropertySheet propertySheet = new PropertySheet();
        propertySheet.setPropertyEditorFactory(new DefaultPropertyEditorFactory()
        {
            @Override
            public PropertyEditor<?> call(PropertySheet.Item item)
            {
                // Class<?> type = item.getType();
                if ("elements".equals(item.getName())) // actually one should check for the type instead of the name
                {
                    return new PropertyEditor<List<String>>()
                    {
                        MultiValueEdit<String> edit = new MultiValueEdit<>();

                        {
                            edit.setOnAddElementAction(evt -> {
                                TextInputDialog dialog = new TextInputDialog(null);
                                dialog.initStyle(StageStyle.UTILITY);
                                dialog.setTitle("Input");
                                dialog.setHeaderText("Enter data");
                                dialog.setContentText("Please enter new value");

                                dialog.showAndWait().ifPresent(newItem -> {
                                    edit.getItems().add(newItem);
                                    updateItem();
                                });
                            });
                            edit.setOnRemoveElementAction(evt -> {
                                edit.getItems().removeAll(edit.getSeletedItems());
                                updateItem();
                            });
                            edit.setOnReorderAction(evt -> {
                                updateItem();
                            });
                        }

                        private void updateItem()
                        {
                            item.setValue(getValue());
                        }

                        @Override
                        public Node getEditor()
                        {
                            return edit;
                        }

                        @Override
                        public List<String> getValue()
                        {
                            return new ArrayList<>(edit.getItems());
                        }

                        @Override
                        public void setValue(List<String> value)
                        {
                            edit.getItems().setAll(value);
                        }
                    };
                }

                return super.call(item);
            }
        });
        DemoBean demoBean = new DemoBean();
        propertySheet.getItems().setAll(BeanPropertyUtils.getProperties(demoBean));

        /*
        MultiValueEdit<String> edit = new MultiValueEdit<>();
        edit.setOnAddElementAction(evt -> {
            System.err.println("Add");

            edit.getItems().add("ABC " + System.currentTimeMillis());
        });
        edit.setOnRemoveElementAction(evt -> {
            System.err.println("Remove");

            edit.getItems().removeAll(edit.getSeletedItems());
        });
        edit.setOnReorderAction(evt -> {
            System.err.println("reordered");

            System.err.println("====");
            edit.getItems().forEach(System.err::println);
            System.err.println("====");
        });
        */

        Scene scene = new Scene(propertySheet, 800, 600);

        primaryStage.setScene(scene);

        primaryStage.show();
    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
